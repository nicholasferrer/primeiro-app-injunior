class Post < ApplicationRecord
  belongs_to :user
  has_many :comentarios
  validates :content, presence: true, lenght: { in: 2..140 }
end
